using UnityEngine;
using UnityEngine.SceneManagement;


public class Menu : MonoBehaviour
{
    void Start()
    {
        
    }

    public void LoadScene(string sceneName = null)
    {
        SceneManager.LoadScene(sceneName);
    }
}
