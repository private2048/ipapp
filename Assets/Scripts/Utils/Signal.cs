﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Signal<T>
{
    private List<Action<T>> onceList;

    private List<Action<T>> list;

    public Signal()
    {
        onceList = new List<Action<T>>();
        list = new List<Action<T>>();
    }

    public void AddOnce(Action<T> f)
    {
        onceList.Add(f);
    }

    public void Add(Action<T> f)
    {
        list.Add(f);
    }

    public void Call(T value)
    {
        Action<T>[] temp = onceList.ToArray();

        foreach (var a in temp) a.Invoke(value);

        onceList.Clear();

        temp = list.ToArray();

        foreach (var a in temp) a.Invoke(value);
    }

    public void Remove(Action<T> a)
    {
        onceList.Remove(a);

        list.Remove(a);
    }

    public void RemoveAll()
    {
        onceList.Clear();

        list.Clear();
    }
}

