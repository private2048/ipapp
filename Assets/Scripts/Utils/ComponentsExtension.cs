﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ComponentsExtension 
{
    private static Game _game;

    public static Game game(this MonoBehaviour m)
    {
        return _game;
    }

    public static void SetValues(this MonoBehaviour m, Game g)
    {
        _game = g;
    }
	
    public static void AddDescendantsWithTag(this Transform parent, string tag, List<GameObject> list)
    {
        foreach (Transform child in parent)
        {
            if (child.gameObject.tag == tag)
            {
                list.Add(child.gameObject);
            }

            AddDescendantsWithTag(child, tag, list);
        }
    }

    public static void AddDescendantsWithName(this Transform parent, string name, List<GameObject> list)
    {
        foreach (Transform child in parent)
        {
            if (child.gameObject.name == name)
            {
                list.Add(child.gameObject);
            }

            AddDescendantsWithName(child, name, list);
        }
    }

    private static System.Random rng = new System.Random();
	
    public static void ShuffleArr<T>(this T[] array)
    {
        rng = new System.Random();
        int n = array.Length;
        while (n > 1)
        {
            int k = rng.Next(n);
            n--;
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }
    }
	
    public static IEnumerable<T> ShuffleEnum<T>(this IEnumerable<T> list)
    {
        var r = new System.Random((int)DateTime.Now.Ticks);
        var shuffledList = list.Select(x => new { Number = r.Next(), Item = x }).OrderBy(x => x.Number).Select(x => x.Item);
        return shuffledList.ToList();
    }
	
    public static T Next<T>(this T src) where T : struct
    {
        if (!typeof(T).IsEnum) throw new ArgumentException(
            String.Format("Argument {0} is not an Enum", typeof(T).FullName));

        T[] Arr = (T[])Enum.GetValues(src.GetType());
        int j = Array.IndexOf<T>(Arr, src) + 1;
        return (Arr.Length == j) ? Arr[0] : Arr[j];
    }
}
