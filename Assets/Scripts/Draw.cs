using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class Draw : MonoBehaviour, IPointerMoveHandler, IPointerDownHandler, IPointerUpHandler
{
    public Texture2D tex;
    public Color startColor;
    public float mul = 10f;

    private int size = 5;
    private Vector2Int drawPoint = new Vector2Int();
    private bool isHavePoint = false;
    private bool isPointerDown = false;
    private bool isPlayReady = true;

    void Start()
    {
        FillColor();
    }

    void FillColor()
    {
        var fillColorArray = tex.GetPixels();

        for (var i = 0; i < fillColorArray.Length; ++i)
        {
            fillColorArray[i] = startColor;
        }

        tex.SetPixels(fillColorArray);

        tex.Apply();
    }

    public void OnPointerMove(PointerEventData eventData)
    {
        isHavePoint = true;

        var wp = eventData.pointerCurrentRaycast.worldPosition; // world point to texture coords
        wp.x += 5f;
        wp.z += 5f;
        float mul = 64f / 5f;
        drawPoint.x = (int)(wp.x * mul);
        drawPoint.y = (int)(wp.z * mul);
    }

    void Update()
    {
        if (!isHavePoint || !isPointerDown) return;

        var c = tex.GetPixel(drawPoint.x, drawPoint.y);
        Color.RGBToHSV(c, out float H, out float S, out float V);
        V -= (V / 50f); //Mathf.Clamp(V * Time.deltaTime * 5f, 0.05f, 0.2f);
        Mathf.Clamp(V, 0f, 1f);
        c = Color.HSVToRGB(H, S, V);
        Circle(tex, drawPoint.x, drawPoint.y, size, c);
        isHavePoint = false;

    }

    public void Circle(Texture2D tex, int cx, int cy, int r, Color col)
    {
        int x, y, px, nx, py, ny, d;

        for (x = 0; x <= r; x++)
        {
            d = (int)Mathf.Ceil(Mathf.Sqrt(r * r - x * x));
            for (y = 0; y <= d; y++)
            {
                px = cx + x;
                nx = cx - x;
                py = cy + y;
                ny = cy - y;

                tex.SetPixel(px, py, col);
                tex.SetPixel(nx, py, col);

                tex.SetPixel(px, ny, col);
                tex.SetPixel(nx, ny, col);
            }
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPointerDown = true;

        if (isPlayReady)
        {
            Play();
        }
    }

    void Play()
    {
        this.game().sfx.DOFade(1f, 0f);
        this.game().Play("Start00");
        Invoke("LoopSound", 1.0f);
        isPlayReady = false;
    }

    void LoopSound()
    {
        //isPlayReady = true;
        this.game().Play("Loop00");
        Invoke("LoopSound", 4.2f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointerDown = false;
        this.game().sfx.DOFade(0f, 1f).OnComplete(()=> 
        {
            this.game().sfx.Stop();
            isPlayReady = true;
        });
    }
}
