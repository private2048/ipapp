using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public AudioClip[] clips;
    public AudioSource sfx;
    public AudioSource music;
    void Start()
    {
        this.SetValues(this);
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public float Play(string name, float vol = 1f, float delay = -1f)
    {
        AudioClip clip = null;

        foreach (var c in clips)
        {
            if (c.name == name)
            {
                clip = c;
                break;
            }
        }

        if (clip == null) return 0;

        if (delay > 0)
        {
            music.transform.DOMoveX(0, delay).OnComplete(() =>
            {
                sfx.PlayOneShot(clip, vol);
            });
        }
        else
        {
            sfx.PlayOneShot(clip, vol);
        }

        return clip.length;
    }
}
