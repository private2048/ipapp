using UnityEngine;
using UnityEngine.UI;

public class Fps : MonoBehaviour
{
    private Text text;
    private int counter = 0;
    void Start()
    {
        text = GetComponent<Text>();
        InvokeRepeating("DrawFps", 0f, 1f);
    }

    void Update()
    {
        counter++;
    }

    void DrawFps()
    {
        text.text = counter.ToString()+"fps";
        counter = 0;
    }
}
